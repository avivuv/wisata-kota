$(function() {
    $('.input-select2').select2();
});

function swalConfirm(msg, action) {
    swal({
      title: msg,
      text: '',
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#dd4b39",
      confirmButtonText: "OK"     
    }, function() {
        if ($.isFunction(action)) {
            action();
        } else {
            document.location.href=action;
        }
    });
}