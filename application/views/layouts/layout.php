<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Wisata Kota</title>
        <link href="<?= base_url('public/plugins/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">    
        <link href="<?= base_url('public/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">    
        <link href="<?= base_url('public/plugins/nprogress/nprogress.css') ?>" rel="stylesheet">    
        <link href="<?= base_url('public/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">    
        <link href="<?= base_url('public/plugins/jquery.growl/css/jquery.growl.css') ?>" rel="stylesheet">    
        <link href="<?= base_url('public/plugins/sweetalert/dist/sweetalert.css') ?>" rel="stylesheet">    
        <link href="<?= base_url('public/plugins/select2/dist/css/select2.css') ?>" rel="stylesheet">    
        <link href="<?= base_url('public/build/css/gentelella.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('public/build/css/layout.css') ?>" rel="stylesheet">        
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?= base_url() ?>" class="site_title"><i class="fa fa-map"></i> <span>Wisata Kota</span></a>
                        </div>
                        <div class="clearfix"></div>                    
                        <br />                    
                        <?php $this->template->view('layouts/partials/sidebar') ?>
                    </div>     
                </div>           
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            
                            <!--<ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <?/*= getLogin('username') */?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">                                        
                                        <li><a href="<?/*= $this->routes->name('setting') */?>"><i class="fa fa-key pull-right"></i><span>{{change_password}}</span></a></li>
                                        <li><a href="<?/*= $this->routes->name('logout') */?>"><i class="fa fa-sign-out pull-right"></i> {{log_out}}</a></li>
                                    </ul>
                                </li> 
                                <li role="presentation" class="dropdown">
                                    <a href="<?/*= $this->routes->name('sgw_inbox') */?>"  class="info-number">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="badge bg-green" id="new_message">0</span>
                                    </a>                                    
                                </li>
                            </ul>-->
                        </nav>
                    </div>
                </div>
                <div class="right_col" role="main">
                    <?php $this->template->render('content') ?>
                </div>
                <footer>
                    <div class="pull-left">
                        &copy; Copyright 2017 D
                    </div>
                    <div class="pull-right">
                        Designed By <a href="https://colorlib.com">Colorlib</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>                
            </div>
        </div>        
        <script src="<?= base_url('public/plugins/jquery/dist/jquery.min.js') ?>"></script>        
        <script src="<?= base_url('public/plugins/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('public/plugins/fastclick/lib/fastclick.js') ?>"></script>        
        <script src="<?= base_url('public/plugins/nprogress/nprogress.js') ?>"></script>        
        <script src="<?= base_url('public/plugins/datatables.net/js/jquery.dataTables.min.js') ?>"></script>   
        <script src="<?= base_url('public/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>   
        <script src="<?= base_url('public/plugins/jquery.growl/js/jquery.growl.js') ?>"></script>        
        <script src="<?= base_url('public/plugins/sweetalert/dist/sweetalert.min.js') ?>"></script>        
        <script src="<?= base_url('public/plugins/select2/dist/js/select2.min.js') ?>"></script>        
        <script src="<?= base_url('public/plugins/bootbox/bootbox.min.js') ?>"></script>    
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXc1GfgBiMrqmXvbKf6HOEWbdcdqN_ZXQ"></script>    
        <script src="<?= base_url('public/build/js/gentelella.js') ?>"></script>
        <script src="<?= base_url('public/build/js/layout.js') ?>"></script>


        <!--<script>
            $(function() {
                new_message();

                setInterval(function () {
                    $.ajax({url: '<?/*= $this->routes->name('sgw_engine_send'); */?>'});
                    $.ajax({url: '<?/*= $this->routes->name('sgw_engine_read'); */?>'});
                    new_message();
                }, 10000);
            });

            function new_message() {
                $.ajax({
                    url: '<?/*= $this->routes->name('sgw_inbox_new'); */?>',
                    dataType: 'json',
                    success: function (response) {
                        $('#new_message').html(response['data']['new_message']);
                        $('#new_message_side').html(response['data']['new_message']);
                    }
                });
            }
        </script>-->

        <?php $this->template->view('layouts/partials/message') ?>
        <?php $this->template->render('page_script') ?>
    </body>
</html>
