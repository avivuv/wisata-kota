<?php if ($this->session->flashdata('successMessage')) { ?>
	<script>
        $.growl.notice({
            title : '{{success}}',
            message : '<?= $this->session->flashdata('successMessage') ?>'
        })
    </script>
<?php } ?>
<?php if ($this->session->flashdata('errorMessage')) { ?>
	<script>
        $.growl.error({
            title : '{{error}}',
            message : '<?= $this->session->flashdata('errorMessage') ?>'
        })
    </script>
<?php } ?>