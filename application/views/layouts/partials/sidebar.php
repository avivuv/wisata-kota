<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">            
            <!--<li><a href="<?/*= $this->routes->name('sgw_phonebook') */?>"><i class="fa fa-users"></i> {{contact}}</a></li>
            <li><a href="<?/*= $this->routes->name('sgw_message_templates') */?>"><i class="fa fa-file-text"></i> {{message_templates}}</a></li>
            <li><a href="<?/*= $this->routes->name('sgw_inbox') */?>"><i class="fa fa-inbox"></i> {{inbox}} <span class="badge bg-green" id="new_message_side">0</span></a></li>-->
            <li><a href="<?= base_url('wisata/wisata') ?>"><i class="fa fa-map"></i> {{map}}</a></li>
            <li><a><i class="fa fa-map-marker"></i> {{wisata}} <span class="fa fa-caret-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?= base_url('wisata/wisata') ?>?type=wisata&category=pantai">{{pantai}}</a></li>  
                    <li><a href="<?= base_url('wisata/wisata') ?>?type=wisata&category=gunung">{{gunung}}</a></li>   
                </ul>
            </li>
            <li><a href="<?= base_url('wisata/wisata') ?>?type=penginapan&category=hotel"><i class="fa fa-bed"></i> {{penginapan}}</a></li>
            <li><a href="<?= base_url('wisata/wisata') ?>?type=kuliner&category=restoran"><i class="fa fa-cutlery "></i> {{kuliner}}</a></li>
            <li><a><i class="fa fa-shopping-cart"></i> {{pasar}} <span class="fa fa-caret-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?= base_url('wisata/wisata') ?>?type=wisata&category=tradiosional">{{tradiosional}}</a></li>  
                    <li><a href="<?= base_url('wisata/wisata') ?>?type=wisata&category=swalayan">{{swalayan}}</a></li>   
                </ul>
            </li>
        </ul>
    </div>    
</div>                       
