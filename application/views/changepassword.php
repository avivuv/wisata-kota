<?php $this->template->section('content') ?>
    <div class="page-title">
        <div class="title_left">
            <h3>{{change_password}}</h3>
        </div>
    </div>
    <div class="x_panel">
        <div class="x_content">
            <?= $this->form->open($this->routes->name('changepassword')) ?>
            <div class="form-group">
                <div class="form-group">
                    <label>{{old_password}}</label>
                    <?= $this->form->password('old_password', null, 'class="form-control"') ?>
                </div>
                <div class="form-group">
                    <label>{{new_password}}</label>
                    <?= $this->form->password('new_password', null, 'class="form-control"') ?>
                </div>
                <div class="form-group">
                    <label>{{confirm_new_password}}</label>
                    <?= $this->form->password('confirm_new_password', null, 'class="form-control"') ?>
                </div>
                <button class="btn btn-default">{{save}}</button>
            </div>
            <?= $this->form->close() ?>
        </div>
    </div>
<?php $this->template->endsection() ?>

<?php $this->template->view('layouts/layout') ?>