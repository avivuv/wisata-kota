<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>E N T E R GateWay</title>
    <link href="<?= base_url('public/plugins/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/plugins/jquery.growl/css/jquery.growl.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/build/css/animate.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/build/css/custom.min.css') ?>" rel="stylesheet">
</head>

<body class="login">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <?= $this->form->open($this->routes->name('authentication')) ?>
                    <h1>Login Form</h1>
                    <div>
                        <input type="text" name="username" class="form-control" placeholder="{{username}}" required="" />
                    </div>
                    <div>
                        <input type="password" name="password" class="form-control" placeholder="{{Password}}" required="" />
                    </div>
                    <div>
                        <button type="submit" class="btn btn-default">{{log_in}}</button>
                    </div>
                    <div class="separator">
                        <div>
                            <h1><i class="fa fa-inbox"></i> E N T E R GateWay</h1>
                            <p>&copy;2017 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                        </div>
                    </div>
                <?= $this->form->close() ?>
            </section>
        </div>
    </div>
</div>
<script src="<?= base_url('public/plugins/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= base_url('public/plugins/jquery.growl/js/jquery.growl.js') ?>"></script>
<?php $this->template->view('layouts/partials/message') ?>
</body>
</html>