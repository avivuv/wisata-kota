<?php $this->template->section('content') ?>
<div class="page-title">
    <div class="title_left">
        <h4>{{tempat_wisata}}</h4>
    </div>    
</div>
<div class="x_panel">    
    <div class="x_content">
        <div id="direction" style="display: none;">
            <img id="direction-map" style="width: 100%; height: 400px">
            <br><br>
            <center>
                <button onclick="showMap()" class="btn btn-defaul btn-lg">{{back}}</button>
            </center>
        </div>
        <div id="loading" class="text-center" style="display: none"><i class="fa fa-spinner fa-spin" style="font-size: 120px"></i></div>
        <div id="map" style="height: 400px; width: 100%"></div>
    </div>
</div>
<?php $this->template->endsection() ?>

<?php $this->template->section('page_script') ?>
    <script>
        $(function() {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: new google.maps.LatLng(-8.0645937, 111.9001348),
                mapTypeId: 'roadmap'
            });

            var iconBase = 'http://localhost/wisata-kota/public/build/icon/';
            var icons = {
                wisata: {
                    pantai: {
                        icon: iconBase + 'beach.png'
                    },
                    gunung: {
                        icon: iconBase + 'mountain.png'
                    }
                },
                penginapan: {
                    hotel: {
                        icon: iconBase + 'hotel-2.png'
                    }
                },
                kuliner: {
                    restoran: {
                        icon: iconBase + 'resto.png'
                    }
                },
                pasar: {
                    oleh_oleh: {
                        icon: iconBase + 'shoping.png'
                    },
                    tradisional: {
                        icon: iconBase + 'shoping.png'
                    },
                    swalayan: {
                        icon: iconBase + 'shoping.png'
                    }
                }

            };

            var directionBase = 'http://localhost/wisata-kota/public/build/direction/';
            var contentString = {
                wisata: {
                    pantai: {
                        kedung_tumpang: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Pantai Kedung Tumpang</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Sebenarnya Pantai Kedung Tumpang merupakan tebing-tebing alami yang mempunyai bentuk berbeda-beda bahkan yang spesial membentuk kolam-kolam kecil dan besar. Tebing yang membentuk kolam ini berisi air laut yang jernih. Tebing yang berwarna coklat keemasan, birunya air laut, dan hijaunya warna lumut yang berada di pinggiran tebing kolam membuatnya semakin indah dinikmati mata. Hempasan ombak yang menghantam tebing-tebing menyisakan buih putih sebelum akhirnya surut dan kembali terhantam ombak.</p>'+
                            '<a href="'+directionBase+'pantai_kedung_tumpang.jpg" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        },
                        gemah: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Pantai Gemah</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Pantai dengan hamparan pasir berwarna kecoklatan ini berada di Desa Keboireng, Kecamatan Besuki, Tulungagung. Pantai Gemah ini juga merupakan salah satu pantai yang mempunyai garis pantai yang cukup panjang. Terlihat di area pinggir pantai ini juga terdapat pohon - pohon cemara yang seakan ikut melengkapi keindahan pantai ini.</p>'+
                            '<a href="'+directionBase+'pantai_gemah.jpg" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        },
                        sanggar: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Pantai Sanggar</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Pantai Sanggar sangat jernih dan bersih karena masih jarang dikunjungi wisatawan. Dengan pesona pasirnya yang putih membuat kita pasti ingin berlama-lama disini. Pada sore hari ketika air laut surut, kita bisa mencari kerang atau mungkin sekedar mengamati kehidupan bawah laut dari balik batu karang.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        }
                    },
                    gunung: {
                        gunung_budeg: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Gunung Budheg</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Jika kita sudah mendaki hingga puncak Gunung Budheg maka kita akan bisa melihat seluruh sudut Kabupaten Tulungagung. Namun jalan menuju ke puncak tidak semudah yang dibayangkan karena mulai dari titik awal perjalanan hingga sampai puncak gunung, kita tidak akan mendapatkan tempat datar untuk beristirahat. Jadi persiapkan fisik untuk mendaki gunung yang menjadi salah satu ikon wisata di Tulungagung ini.</p>'+
                            '<a href="'+directionBase+'gunung_budeg.jpg" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        }
                    }
                },
                penginapan: {
                    hotel: {
                        crown_victoria: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Crown Victoria Hotel</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Crown Victoria Hotel terletak di jantung kota Tulungagung, cuma 5 menit berkendara dari stasiun kereta api Tulungagung. Hotel ini memiliki kolam renang indoor dan kamar-kamar ber-AC dengan TV kabel layar datar. Wi-Fi gratis tersedia di seluruh bangunan. Dilengkapi dengan interior modern, setiap kamar dilengkapi dengan meja dan lemari.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        },
                        istana: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Istana Hotel</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Hotel ini berjarak hanya 5 menit berkendara dari Stasiun Kereta Tulungagung dan pasar tradisional Tulungagung. Menawarkan Wi-Fi gratis, hotel juga memiliki sebuah pusat kebugaran dan spa. Kamar-kamar kontemporer hotel ini memiliki lantai kayu, dan dihias dengan warna-warna netral. Kamar menawarkan TV layar datar dengan saluran kabel, fasilitas membuat teh/kopi, dan minibar.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        }
                    }
                },
                kuliner: {
                    restoran: {
                        ayam_lodho: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Ayam Lodho</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Menu Andalan : Lodho Ayam Kampung Khas Tulungagung.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        },
                        waroeng_bima: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Waroeng Bima</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Menu Andalan : Western Food, Chinesse Food, Indonesian Food.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        },
                        jepun_view_resto: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Jepun View Resto</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Menu Andalan : Ayam Bakar Jepun, Orak Arik Pedho Pete.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        }
                    }
                },
                pasar: {
                    tradisional: {
                        pasar_wage: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Pasar Wage Tulungagung</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Pasar Wage Tulungagung.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        },
                        pasar_ngemplak: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Pasar Ngemplak Tulungagung</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Pasar Ngemplak Tulungagung.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        }
                    },
                    swalayan: {
                        golden_swalayan: {
                            content: '<div id="content">'+
                            '<div id="siteNotice">'+
                            '</div>'+
                            '<h4 id="firstHeading" class="firstHeading">Golden Swalayan Tulungagung</h4>'+
                            '<div id="bodyContent">'+
                            '<p>Golden Swalayan Tulungagung.</p>'+
                            '<a href="#" class="btn btn-primary btn-xs">Petunjuk Arah</a>'+
                            '<a href="#" class="btn btn-primary btn-xs">Detail</a>'+
                            '</div>'+
                            '</div>'
                        }
                    }
                }
            };

            var features = [
                {
                    position: new google.maps.LatLng(-8.3033462, 112.010243),
                    type: 'wisata',
                    category: 'pantai',
                    name: 'kedung_tumpang',
                    title:'Pantai Kedung Tumpang',                                
                    rate : '4,9',
                    info : {
                        HTM : 'Rp 5.000',
                        Deskripsi : 'Sebenarnya Pantai Kedung Tumpang merupakan tebing-tebing alami yang mempunyai bentuk berbeda-beda bahkan yang spesial membentuk kolam-kolam kecil dan besar. Tebing yang membentuk kolam ini berisi air laut yang jernih. Tebing yang berwarna coklat keemasan, birunya air laut, dan hijaunya warna lumut yang berada di pinggiran tebing kolam membuatnya semakin indah dinikmati mata. Hempasan ombak yang menghantam tebing-tebing menyisakan buih putih sebelum akhirnya surut dan kembali terhantam ombak.'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.2643909, 111.7711432),
                    type: 'wisata',
                    category: 'pantai',
                    name: 'gemah',
                    title:'Pantai Gemah',                    
                    rate : '4,2',
                    info : {                         
                        Deskripsi : 'Pantai dengan hamparan pasir berwarna kecoklatan ini berada di Desa Keboireng, Kecamatan Besuki, Tulungagung. Pantai Gemah ini juga merupakan salah satu pantai yang mempunyai garis pantai yang cukup panjang. Terlihat di area pinggir pantai ini juga terdapat pohon – pohon cemara yang seakan ikut melengkapi keindahan pantai ini.'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.297335, 111.9102585),
                    type: 'wisata',
                    category: 'pantai',
                    name: 'sanggar',
                    title:'Pantai Sanggar',                    
                    rate : '4,4',
                    info : {                         
                        Deskripsi : 'Pantai dengan hamparan pasir berwarna kecoklatan ini berada di Desa Keboireng, Kecamatan Besuki, Tulungagung. Pantai Gemah ini juga merupakan salah satu pantai yang mempunyai garis pantai yang cukup panjang. Terlihat di area pinggir pantai ini juga terdapat pohon – pohon cemara yang seakan ikut melengkapi keindahan pantai ini.'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.1357927, 111.906752),
                    type: 'wisata',
                    category: 'gunung',
                    name: 'gunung_budeg',
                    title:'Gunung Budeg',                    
                    rate : '4,9',
                    info : {                         
                        Deskripsi : 'Jika kita sudah mendaki hingga puncak Gunung Budheg maka kita akan bisa melihat seluruh sudut Kabupaten Tulungagung. Namun jalan menuju ke puncak tidak semudah yang dibayangkan karena mulai dari titik awal perjalanan hingga sampai puncak gunung, kita tidak akan mendapatkan tempat datar untuk beristirahat. Jadi persiapkan fisik untuk mendaki gunung yang menjadi salah satu ikon wisata di Tulungagung ini.'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.0741744, 111.9042649),
                    type: 'penginapan',
                    category: 'hotel',
                    name: 'crown_victoria',
                    title:'Crown Victoria Hotel',                    
                    rate : '4,7',
                    info : {   
                        Harga : 'Rr 229.000 - 749.000',
                        Telp : '0355-331321',
                        Deskripsi : 'berkendara dari stasiun kereta api Tulungagung. Hotel ini memiliki kolam renang indoor dan kamar-kamar ber-AC dengan TV kabel layar datar. Wi-Fi gratis tersedia di seluruh bangunan. Dilengkapi dengan interior modern, setiap kamar dilengkapi dengan meja dan lemari. Kamar tertentu memiliki minibar. Staf di meja depan 24-jam tersedia untuk membantu para tamu dengan berbagai layanan, seperti layanan binatu dan menyetrika. Para tamu dapat berolahraga di pusat kebugaran atau bersantai di pusat spa & kesehatan. Layanan penyewaan mobil disediakan dengan biaya tambahan. Parkir gratis tersedia di hotel. Sultan Café & Resto menyajikan masakan Indonesia, masakan Barat dan Cina. Para tamu dapat menikmati suasana pantai sambil makan di Venezia Kedua Resto dan Pool Bar, yang menyajikan masakan internasional.'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.0605674, 111.9018751),
                    type: 'penginapan',
                    category: 'hotel',
                    name: 'istana',
                    title:'Hote Istana',                    
                    rate : '4,7',
                    info : {   
                        Harga : 'Rr 200.000 - 550.000',
                        Telp : '0355-332377',
                        Deskripsi : 'Hotel ini berjarak hanya 5 menit berkendara dari Stasiun Kereta Tulungagung dan pasar tradisional Tulungagung. Menawarkan Wi-Fi gratis, hotel juga memiliki sebuah pusat kebugaran dan spa. Kamar-kamar kontemporer hotel ini memiliki lantai kayu, dan dihias dengan warna-warna netral. Kamar menawarkan TV layar datar dengan saluran kabel, fasilitas membuat teh/kopi, dan minibar. Perlengkapan mandi dan pengering rambut tersedia di kamar mandi dalam. Kartika Sari Restaurant menyajikan hidangan ala carte Indonesia yang lezat di samping masakan Cina. Layanan kamar juga tersedia. Istana Hotel menyediakan meja depan 24 jam yang dapat membantu dengan layanan binatu dan penyewaan mobil. Fasilitas rapat/perjamuan dan pusat bisnis juga tersedia.'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.0798213, 111.9309071),
                    type: 'kuliner',
                    category: 'restoran',
                    name: 'ayam_lodho',
                    title:'Warung Ayam Lodho Sumber Rejeki',                    
                    rate : '4,9',
                    info : {   
                        Harga : 'Rr 10.000',
                        Buka : '09.00 WIB - 21.00 WIB',
                        'Menu Andalan' : 'Lodho Ayam Kampung Khas Tulungagung.'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.0685804, 111.8958701),
                    type: 'kuliner',
                    category: 'restoran',
                    name: 'waroeng_bima',
                    title:'Warung Bima',                    
                    rate : '4.0',
                    info : {   
                        Harga : 'Rr 10.000 - 50.000',
                        Telp : '0355-321489',
                        Buka : '09.00 WIB - 21.00 WIB',                        
                        'Menu Andalan' : 'Western Food, Chinesse Food, Indonesian Food'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.0743124, 111.9104425),
                    type: 'kuliner',
                    category: 'restoran',
                    name: 'jepun_view_resto',
                    title:'Warung Bima',                    
                    rate : '4.2',
                    info : {   
                        Harga : 'Rr 10.000 - 50.000',
                        Telp : '0355-320711',
                        Buka : '10.00 WIB - 22.00 WIB',                        
                        'Menu Andalan' : 'Ayam Bakar Jepun, Orak Arik Pedho Pete'
                    }
                },
                {
                    position: new google.maps.LatLng(-8.0577762, 111.8997802),
                    type: 'pasar',
                    category: 'tradisional',
                    name: 'pasar_wage',
                    title : 'Pasar Wage'
                },
                {
                    position: new google.maps.LatLng(-8.0548753, 111.891982),
                    type: 'pasar',
                    category: 'tradisional',
                    name: 'pasar_ngemplak',
                    title : 'Pasar Ngemplak'
                },
                {
                    position: new google.maps.LatLng(-8.0652748, 111.9041491),
                    type: 'pasar',
                    category: 'swalayan',
                    name: 'golden_swalayan',
                    title : 'Golden'
                }
            ];

            // Create markers.            
            features.forEach(function(feature) {                
                <?php if ($this->input->get('type')) { ?>
                    if (feature.type == '<?= $this->input->get('type') ?>') {
                <?php } ?>   
                    <?php if ($this->input->get('category')) { ?>
                        if (feature.category == '<?= $this->input->get('category') ?>') {
                    <?php } ?>                  
                        var marker = new google.maps.Marker({
                            animation: google.maps.Animation.DROP,
                            position: feature.position,
                            icon: icons[feature.type][feature.category].icon,
                            map: map
                        });                
                        var infowindowContent = '<div id="content" style="">';
                        infowindowContent +='<div id="siteNotice">';
                        infowindowContent +='</div>';
                        infowindowContent +='<h4 id="firstHeading" class="firstHeading">'+feature.title+' <span class="text-warning"><i class="fa fa-star"></i> '+feature.rate+'</span></h4>';    
                        infowindowContent +='<div id="bodyContent">';                                                                    
                        infowindowContent +='<a href="#" class="btn btn-primary btn-xs" onclick="direction(\''+directionBase+feature.name+'.jpg\')">Petunjuk Arah</a>';
                        infowindowContent +='<a href="<?= base_url('wisata/wisata/kedung_tumpang') ?>" class="btn btn-primary btn-xs">Detail</a>';
                        if (feature.info) {
                            $.each(feature.info, function(key, info) {
                                infowindowContent += '<p><b>'+key+'</b><br>'+info+'</p>';
                            });
                        }
                        infowindowContent +='</div>';
                        infowindowContent +='</div>';
                        var infowindow = new google.maps.InfoWindow({
                            content: infowindowContent
                        });

                        marker.addListener('click', function () {
                            //map.setZoom(11);
                            map.setCenter(marker.getPosition());
                            infowindow.open(map, marker);
                        });
                    <?php if ($this->input->get('category')) { ?>
                        }
                    <?php } ?>
                <?php if ($this->input->get('type')) { ?>
                    }
                <?php } ?>                
            });

        });
        function direction(url) {
            $('#map').hide();
            $('#loading').show();
            setTimeout(function() {
                $('#loading').hide();
                $('#direction-map').attr('src', url);
                $('#direction').show();
            }, 2000);
        }

        function showMap() {
            $('#direction').hide();
            $('#map').show();
        }
    </script>
<?php $this->template->endsection() ?>

<?php $this->template->view('layouts/layout') ?>