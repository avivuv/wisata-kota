<?php $this->template->section('content') ?>
<div class="page-title">
    <div class="title_left">
        <h4>{{tempat_wisata}}</h4>
    </div>    
</div>
<div class="x_panel">    
    <div class="x_content">
        <img src="<?= base_url('public/build/images/kedung_tumpang/1.jpg') ?>" style="width:100%">
        <h1>Pantai Kedung Tumpang</h1>
        4,9 <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half"></i> 317 reviews<br>
        <label class="label label-primary">Wisata</label> <label class="label label-info">Pantai</label>
        <br><br>
        <p>
            <i class="fa fa-map-marker"></i>
            Desa Pucanglaban | Kec.Pucanglaban | Kab.Tulungagung | Jawa Timur
        </p>        
        <p>
            <i class="fa fa-ticket"></i>
            Harga Tiket Masuk : Rp 5.000
        </p>
        <p>
            <i class="fa fa-search"></i> <b>Deskripsi</b><br>
            Sebenarnya Pantai Kedung Tumpang merupakan tebing-tebing alami yang mempunyai bentuk berbeda-beda bahkan yang spesial membentuk kolam-kolam kecil dan besar. Tebing yang membentuk kolam ini berisi air laut yang jernih. Tebing yang berwarna coklat keemasan, birunya air laut, dan hijaunya warna lumut yang berada di pinggiran tebing kolam membuatnya semakin indah dinikmati mata. Hempasan ombak yang menghantam tebing-tebing menyisakan buih putih sebelum akhirnya surut dan kembali terhantam ombak.            
        </p>
        <p>
            <i class="fa fa-photo"></i> <b>Photos</b><br>
            <div class="row">
                <div class="col-sm-4">
                    <img src="<?= base_url('public/build/images/kedung_tumpang/1.jpg') ?>" class="img-responsive">
                </div>
                <div class="col-sm-4">
                    <img src="<?= base_url('public/build/images/kedung_tumpang/2.jpg') ?>" class="img-responsive">
                </div>
                <div class="col-sm-4">
                    <img src="<?= base_url('public/build/images/kedung_tumpang/3.jpg') ?>" class="img-responsive">
                </div>
            </div>
        </p>
    </div>
</div>
<?php $this->template->endsection() ?>


<?php $this->template->view('layouts/layout') ?>