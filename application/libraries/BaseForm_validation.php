<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseForm_validation extends CI_Form_validation {

    public function set_rules($name, $label = '', $rules = array(), $errors = array()) {
        parent::set_rules($name, $this->CI->localization->get($label), $rules, $errors);
    }

}