<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response {

    private $CI;    
    private $contetType = 'application/json';
    protected $with = array();

    public function __construct() {
        $this->CI = & get_instance();
    }

    public function generate($success, $status, $data = null, $attributes = null, $check_number = true) {
        $response = array();
        $response['success'] = $success;
        $response['status'] = $status;
        $response['data'] = $data;
        foreach ($this->with as $key => $value) {
            $response[$key] = $value;
        }
        if ($attributes) {
            foreach ($attributes as $key => $attribute) {
                $response[$key] = $attribute;
            }
        }
        if ($check_number) {
            $this->CI->output->set_content_type($this->contetType)->set_output(json_encode($response, JSON_PRETTY_PRINT|JSON_NUMERIC_CHECK))->_display();
        } else {
            $this->CI->output->set_content_type($this->contetType)->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
        }
        exit();
    }

    public function with($name, $value) {
        if (is_array($name)) {
            foreach ($name as $key => $val) {
                $this->with[$key] = $val;
            }
        } else {
            $this->with[$name] = $value;
        }
        return $this;
    }

    public function validation() {
        $this->with['validationMessage'] = $this->CI->form_validation->errors();
        return $this;  
    }

    public function success($msg) {
        $this->with['successMessage'] = $msg;
        return $this;  
    }

    public function error($msg) {
        $this->with['errorMessage'] = $msg;
        return $this;  
    }    

}