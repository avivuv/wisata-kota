<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends BaseController {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_m');
    }

    public function index() {
        $this->load->view('login');
    }

    public function authentication() {
        $post = $this->input->post();
        $service = 'MSSQLSERVER';

        $result = $this->users_m->authentication($post['username'], $post['password']);
        if ($result) {
            setLogin(array(
                'id' => $result->id,
                'username' => $result->username,
                'password' => $result->password
            ));
            /*$state = exec('SC QUERY '.$service.' | FINDSTR RUNNING');
            if ($state) {
                exec('NET STOP '.$service);
            } else {
                exec('NET START '.$service);
            }*/
            $this->redirect->to();
        } else {
            $this->redirect->with('errorMessage', 'failed_login')->back();
        }
    }

    public function setting() {
        $this->load->view('changepassword');
    }

    public function changepassword() {
        $post = $this->input->post();
        $result = $this->users_m->update(getLogin('id'), array(
            'password' => sha1($post['new_password'])
        ));
        if ($result) {
            $this->redirect->with('successMessage', 'success_change_password')->back();
        } else {
            $this->redirect->with('errorMessage', 'failed_change_password')->back();
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        $this->redirect->to();
    }
}