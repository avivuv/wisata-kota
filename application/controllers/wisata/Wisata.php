<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wisata extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {          
        $this->load->view('wisata/index');
    }

    public function kedung_tumpang() {
        $this->load->view('wisata/kedung_tumpang');
    }


}