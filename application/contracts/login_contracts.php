<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_contracts extends Contracts {

    public function changepassword() {
        if ($post = $this->contracts->input->post()) {
            $this->contracts->load->library('form_validation');
            if ($post['old_password']) {
                $this->contracts->load->model('users_m');
                $old = $this->contracts->users_m->find_by('username', getLogin('username'));
                if ($old->password != sha1($post['old_password'])) {
                    $this->contracts->form_validation->set_rules('old_password', 'old_password', 'wrong_old_password',
                        array(
                            'wrong_old_password' => 'The {field} does not match.'
                        )
                    );
                }
            } else {
                $this->contracts->form_validation->set_rules('old_password', 'old_password', 'required');
            }
            $this->contracts->form_validation->set_rules('new_password', 'new_password', 'required|min_length[5]');
            $this->contracts->form_validation->set_rules('confirm_new_password', 'confirm_new_password', 'required|matches[new_password]');
            if (!$this->contracts->form_validation->run()) {
                $this->contracts->redirect->withInput()->withValidation()->back();
            }
        }
    }
}
