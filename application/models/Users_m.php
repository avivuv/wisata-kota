<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_m extends BaseModel {

    protected $table = 'users';
    protected $primary_key = 'id';
    protected $fillable = array('password');

    public function authentication($username, $password) {
        return $this->db->where('username', $username)
            ->where('password', sha1($password))
            ->get($this->table)
            ->row();
    }

}