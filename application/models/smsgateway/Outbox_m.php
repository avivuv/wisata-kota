<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outbox_m extends BaseModel {

    protected $table = 'outbox';
    protected $primary_key = 'ID';
    protected $fillable = array('DestinationNumber', 'UDH', 'TextDecoded', 'MultiPart', 'CreatorID');

    public function __contruct() {
        parent:: __construct();
        $this->set_default = array(
            'CreatorID' => 'Gammu'
        );
    }

    public function get() {
        $this->db->select('outbox.*, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = outbox.DestinationNumber', 'left');
        return parent::get();
    }

    public function get_multipart($udh) {
        return $this->db->where('LEFT(UDH, 10) = ', substr($udh, 0, 10))
            ->get('outbox_multipart')
            ->result();
    }

    public function find($id) {
        $this->db->select('outbox.*, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = outbox.DestinationNumber', 'left');
        return parent::find($id);
    }

    public function find_by($column, $key) {
        $this->db->select('outbox.*, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = outbox.DestinationNumber', 'left');
        return parent::find_by($column, $key);
    }

    public function send($DestinationNumber, $Message) {
        $this->db->trans_begin();
            $numb_of_messages = ceil(strlen($Message)/153);
            $message_part  = str_split($Message, 153);

            $multipart = 'false';
            if ($numb_of_messages > 1) {
                $multipart = 'true';
            }

            $udh = "050003A7" . sprintf("%02s", $numb_of_messages) . "01";
            $this->insert(array(
                'DestinationNumber' => $DestinationNumber,
                'UDH' => $udh,
                'TextDecoded' => $message_part[0],
                'MultiPart' => $multipart,
            ));

            if ($numb_of_messages > 1) {
                $record_multipart = array();
                for ($i=2; $i<=$numb_of_messages; $i++) {
                    $udh = "050003A7".sprintf("%02s", $numb_of_messages).sprintf("%02s", $i);
                    $record_multipart[] = array(
                        'UDH' => $udh,
                        'TextDecoded' => $message_part[$i-1],
                        'ID' => $this->insert_id(),
                        'SequencePosition' => $i
                    );
                }
                $this->db->insert_batch('outbox_multipart', $record_multipart);
            }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}