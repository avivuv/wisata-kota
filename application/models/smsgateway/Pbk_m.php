<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pbk_m extends BaseModel {

    protected $table = 'pbk';
    protected $primary_key = 'ID';
    protected $fillable = array('Name', 'Number');
    protected $order_by = 'Name';
    protected $order = 'asc';   

    public function get()
    {
        $this->db->select('pbk.*, group_concat(pbk_groups.Name separator \', \') as \'Group\'')
        ->join('pbk_group_contacts', 'pbk_group_contacts.id_pbk = pbk.ID', 'left')
        ->join('pbk_groups', 'pbk_groups.ID = pbk_group_contacts.id_pbk_group', 'left')
        ->group_by('pbk_group_contacts.id_pbk, pbk.Name, pbk.Number');        
        return parent::get();
    } 

    public function get_pbk_group_contacts() {
        $this->db->select('pbk.*, pbk_group_contacts.id_pbk_group, pbk_groups.Name as \'Group\'')
        ->join('pbk_group_contacts', 'pbk_group_contacts.id_pbk = pbk.ID')
        ->join('pbk_groups', 'pbk_groups.ID = pbk_group_contacts.id_pbk_group');        
        return parent::get();
    }

    public function insert($record) {
        if (isset($record['Number'])) {
            $record['Number'] = sgw_phonenumber_format($record['Number']);
        }        
        parent::insert($record);
        if (isset($record['GroupID'])) {
            if ($record['GroupID'] <> -1) {
                $this->load->model('pbk_groups_m');
                $contacts[] = $this->insert_id();                
                $this->pbk_groups_m->add_contacts($record['GroupID'], $contacts);
            }
        }
        return true;
    }

    public function update($id, $record) {
        if (isset($record['Number'])) {
            $record['Number'] = sgw_phonenumber_format($record['Number']);
        }
        return parent::update($id, $record);
    }

    public function delete($id) {
        parent::delete($id);
        $this->db->where('id_pbk', $id)
        ->delete('pbk_group_contacts');
        return true;
    }

    public function get_search_contacts($key) {
        return $this->db->query('
            select ID, Name, \'\' as Number, \'group\' as Type, \'\' as GroupID from pbk_groups
            where Name like \'%'.$key.'%\'            
            union all
            select ID, Name, Number, \'person\' as Type, GroupID from pbk
            where Name like \'%'.$key.'%\'
            or Number like \'%'.$key.'%\'
        ')->result();
    }

}