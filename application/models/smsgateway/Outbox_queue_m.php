<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outbox_queue_m extends BaseModel {

    protected $table = 'outbox_queue';
    protected $primary_key = 'ID';
    protected $fillable = array('DestinationNumber', 'Message', 'Status');
    protected $set_default = array(
        'Status' => 0
    );

    public function get_limit($limit) {
        $this->db->order_by('ID', 'ASC')->limit($limit);
        return parent::get();
    }
}