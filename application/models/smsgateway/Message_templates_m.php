<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message_templates_m extends BaseModel {

    protected $table = 'message_templates';
    protected $primary_key = 'id';
    protected $fillable = array('name', 'message');
    protected $order = 'desc';

    public function __contruct() {
        parent:: __construct();
    }

    public function find_by_name($name) {
        return $this->db->where('lower(name)', $name)
        ->get('template_messages')        
        ->row();
    }

}