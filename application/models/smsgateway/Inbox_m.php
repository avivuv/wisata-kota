<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inbox_m extends BaseModel {

    protected $table = 'inbox';
    protected $primary_key = 'ID';
    protected $fillable = array('StatusRead', 'Processed');
    protected $order_by = 'ReceivingDateTime';
    protected $order = 'desc';

    public function get() {
        $this->db->select('inbox.*, pbk.Name as SenderName')
        ->join('pbk', 'pbk.Number = inbox.SenderNumber', 'left');
        return parent::get();
    }

    public function find($id) {
        $this->db->select('inbox.*, pbk.Name as SenderName')
        ->join('pbk', 'pbk.Number = inbox.SenderNumber', 'left');
        return parent::find($id);
    }

    public function find_by($column, $key) {
        $this->db->select('inbox.*, pbk.Name as SenderName')
        ->join('pbk', 'pbk.Number = inbox.SenderNumber', 'left');
        return parent::find_by($column, $key);
    }

    public function read($id) {
        return $this->update($id, array(
            'StatusRead' => 1
        ));
    }

    public function new_message() {
        $this->db->select('COUNT(ID) AS new_message');
        return parent::find_by('StatusRead', 0);
    }
    
}