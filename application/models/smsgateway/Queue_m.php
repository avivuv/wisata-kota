<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Queue_m extends BaseModel {

    protected $table = 'outbox_queue';
    protected $primary_key = 'ID';
    protected $order_by = 'CreatedAt';
    protected $order = 'desc';

    public function __contruct() {
        parent:: __construct();
    }

    public function get() {
        $this->db->select('outbox_queue.*, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = outbox_queue.DestinationNumber', 'left');
        return parent::get();
    }

    public function find($id) {
        $this->db->select('outbox_queue.*, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = outbox_queue.DestinationNumber', 'left');
        return parent::find($id);
    }

    public function find_by($column, $key) {
        $this->db->select('outbox_queue.*, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = outbox_queue.DestinationNumber', 'left');
        return parent::find_by($column, $key);
    }
}