<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pbk_groups_m extends BaseModel {

    protected $table = 'pbk_groups';
    protected $primary_key = 'ID';
    protected $fillable = array('Name');
    protected $order_by = 'Name';
    protected $order = 'asc';

    public function get() {
        $this->db->select('*, (select count(*) from pbk_group_contacts where id_pbk_group = pbk_groups.ID) as num_of_pbk');
        return parent::get();
    }

    public function get_contact($key) {
        $this->db->select('pbk.*')
            ->join('pbk_group_contacts', 'pbk_group_contacts.id_pbk_group = pbk_groups.ID')
            ->join('pbk', 'pbk.ID = pbk_group_contacts.id_pbk')
            ->where('LOWER(pbk_groups.name)', strtolower($key));
        return parent::get();
    }

    public function get_contacts($id_pbk_group) {
        return $this->db->select('pbk.*')
        ->where('id_pbk_group', $id_pbk_group)
        ->join('pbk', 'pbk.ID = pbk_group_contacts.id_pbk')
        ->get('pbk_group_contacts')
        ->result();
    }

    public function find_by_name($name) {
        return $this->db->where('lower(Name)', $name)
        ->get('pbk_groups')
        ->row();
    }

    public function add_contacts($id_pbk_group, $contacts) {
        $record = array();
        $rs_contacts = array();
        foreach ($this->get_contacts($id_pbk_group) as $contact) {
            $rs_contacts[$contact->ID] = $contact->Number;
        }
        foreach ($contacts as $contact) {
            if (!isset($rs_contacts[$contact])) {
                $record[] = array(
                    'id_pbk_group' => $id_pbk_group,
                    'id_pbk' => $contact
                );
            }
        }
        return $this->db->insert_batch('pbk_group_contacts', $record);
    }

    public function remove_contacts($id_pbk_group, $id_pbk) {
        return $this->db->where('id_pbk_group', $id_pbk_group)
        ->where('id_pbk', $id_pbk)
        ->delete('pbk_group_contacts');        
    }

    public function delete($id) {
        parent::delete($id);
        $this->db->where('id_pbk_group', $id)->delete('pbk_group_contacts');
        return true;
    }

}