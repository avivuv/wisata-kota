<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sentitems_m extends BaseModel {

    protected $table = 'sentitems';
    protected $primary_key = 'ID';
    protected $order_by = 'SendingDateTime';
    protected $order = 'desc';

    public function __contruct() {
        parent:: __construct();
    }

    public function get($key = array()) {
        $this->db->select('sentitems.*, group_concat(TextDecoded order by SequencePosition asc separator \'\') as Message, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = sentitems.DestinationNumber', 'left')
            ->where_in('Status', $key)
            ->group_by('ID');
        return parent::get();
    }

    public function find($id) {
        $this->db->select('sentitems.*, group_concat(TextDecoded order by SequencePosition asc separator \'\') as Message, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = sentitems.DestinationNumber', 'left')
            ->group_by('ID');
        return parent::find($id);
    }

    public function find_by($column, $key) {
        $this->db->select('sentitems.*, group_concat(TextDecoded order by SequencePosition asc separator \'\') as Message, pbk.Name as DestinationName')
            ->join('pbk', 'pbk.Number = sentitems.DestinationNumber', 'left')
            ->group_by('ID');
        return parent::find_by($column, $key);
    }
}