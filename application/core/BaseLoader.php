<?php
class BaseLoader extends CI_Loader {

    protected $loader = array();

    public function __construct() {
        parent::__construct();                    
    }

    public function view($view, $vars = array(), $return = false) {                               
        $output = parent::view($view, $vars, true);        
        preg_match_all('/\{{[a-zA-Z0-9_]*\}}/', $output, $langs);                
        foreach ($langs[0] as $lang) {            
            $label = $this->localization->get(str_replace(array('{', '}'), '', $lang));                        
            $output = str_replace($lang, $label, $output);
        }
        $output = trim($output);
        if ($return) {
            return $output;
        } else {            
            echo $output;            
        }
    }    

    public function model($model, $name = '', $db_conn = FALSE) {        
        return parent::model($model, $name, $db_conn);
    }

}