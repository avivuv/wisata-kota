<?php
class BaseController extends CI_Controller {	    

	protected $middleware = array();

	public function __construct() {
		parent::__construct();		    
        $this->load->config('middleware');
        $this->middleware = $this->config->item('middleware');
		$this->middleware();
		$this->contracts();
        $this->load->library('../exceptions/exceptions');
        $this->load->library('../exceptions/model_exceptions');        
	}


	public function middleware() {
		$access['directory'] = trim($this->router->directory, '/');
		$access['class'] = trim($this->router->fetch_class(), '/');
		$access['method'] = trim($this->router->fetch_method(), '/');
		$access = trim(implode('/', $access), '/');		
		if (isset($this->middleware[$access])) {		        
			foreach ($this->middleware[$access] as $method => $data) {
				if ($data) {					
					$this->{$method}($data);
				} else {
					$this->{$method}();
				}
			}
		}
	}

	public function contracts() {
		$this->load->library('../contracts/contracts');
		$directory = $this->router->directory;
		$class = $this->router->fetch_class();		
		$method = $this->router->fetch_method();		
		if (file_exists(APPPATH . '/contracts/' . $directory . ucwords($class) . '_contracts.php')) {
			$contracts = strtolower($class . '_contracts');
			$this->load->library('../contracts/' . $directory . $contracts);							
			if (method_exists($this->{$contracts}, $method)) {				
				$params = array_slice($this->uri->rsegment_array(), 2);				
				call_user_func_array(array($this->{$contracts}, $method), $params);				
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public function has_login() {
		if (isLogin()) {            
			return true;
		} else {
            $this->redirect->guest($this->routes->name('login'));
		}
	}

	public function authorization($access_key) {					
		return true;
	}

}