<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function sgw_phonenumber_format($number) {
    if (substr($number, 0, 1) == '0') {
        return $number = '+62'.substr($number, 1, strlen($number));
    } else {
        return $number;
    }
}

function sgw_pbk_groups_lists($placeholder = null, $placeholder_value = '') {
    $CI = &get_instance();
    $result = $CI->db->get('pbk_groups')->result();
    return lists($result, 'ID', 'Name', $placeholder, $placeholder_value);
}

function sgw_pbk_lists($placeholder = null, $placeholder_value = '') {
    $CI = &get_instance();
    $result = $CI->db->get('pbk')->result();
    return lists($result, 'ID', 'Name', $placeholder, $placeholder_value);
}

function sgw_contacts_lists($placeholder = null, $placeholder_value = '') {
    $CI = &get_instance();
    $result = $CI->db->query('
        select CONCAT(\'G:\',ID) as ID, CONCAT(\'G:\',Name) as Name from pbk_groups
        union all
        select CONCAT(\'P:\', ID) as ID, Name from pbk
    ')->result();
    return lists($result, 'ID', 'Name', $placeholder, $placeholder_value);
}