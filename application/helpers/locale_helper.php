<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function lists_month($placeholder = null, $placeholder_value = '') {
    if ($placeholder) {
        $lists[$placeholder_value] = $placeholder;
    }
    $lists['01'] = locale()->get('locale:month_01');
    $lists['02'] = locale()->get('locale:month_02');
    $lists['03'] = locale()->get('locale:month_03');
    $lists['04'] = locale()->get('locale:month_04');
    $lists['05'] = locale()->get('locale:month_05');
    $lists['06'] = locale()->get('locale:month_06');
    $lists['07'] = locale()->get('locale:month_07');
    $lists['08'] = locale()->get('locale:month_08');
    $lists['09'] = locale()->get('locale:month_09');
    $lists['10'] = locale()->get('locale:month_10');
    $lists['11'] = locale()->get('locale:month_11');
    $lists['12'] = locale()->get('locale:month_12');    
    return $lists;
}

function lists_year($placeholder = null, $placeholder_value = '') {
    $lists = array();
    if ($placeholder) {
        $lists[$placeholder_value] = $placeholder;
    }    
    for ($y = 2015; $y<=date('Y'); $y++) {
        $lists[$y] = $y;
    }
    return $lists;
}

/*function locale_date($timestamp, $format = null) {
    $timestamp = strtotime($timestamp);
    if (!$format) {
        $format = locale()->get('locale:date_format');
    }
    if ($time = $timestamp) {
        return date($format, $time);
    } else {
        return null;
    }
}

function locale_time($timestamp, $format = null) {  
    $timestamp = strtotime($timestamp); 
    if (!$format) {
        $format = locale()->get('locale:time_format');
    }
    if ($time = $timestamp) {
        return date($format, $time);
    } else {
        return null;
    }
}

function locale_datetime($timestamp) {  
    $timestamp = strtotime($timestamp); 
    if ($time = $timestamp) {
        return date(locale()->get('locale:datetime_format'), $time);
    } else {
        return null;
    }
}

function locale_number($number) {
    $number = number_format($number);
    $parse = explode($number, '.');
    $result = str_replace(',', locale()->get('locale:thousand_separator'), $parse[0]);
    if (isset($parse[1])) {
        $result .= locale()->get('locale:decimal_separator') . $parse[1];
    }
    return $result;
}

function locale_currency($str) {    
    return locale()->get('locale:currency').locale_number($str);
}

function locale_human_date($timestamp, $format = null) {    
    $timestamp = strtotime($timestamp);
    if ($time = $timestamp) {
        $y = date('Y', $time);
        $m = locale()->get('locale:month_' . date('m', $time));
        $d = date('d', $time);
        $H = date('H', $time);
        $i = date('i', $time);
        $s = date('s', $time);
        if (!$format) {
            $humanDate = locale()->get('locale:human_date_format');
        } else {
            $humanDate = $format;
        }
        $humanDate = str_replace(array('{Y}', '{m}', '{d}'), array($y, $m, $d), $humanDate);
        return $humanDate;
    } else {
        return null;
    }
}

function locale_human_datetime($timestamp) {    
    $timestamp = strtotime($timestamp);
    if ($time = $timestamp) {
        $y = date('Y', $time);
        $m = getConfig('_month')[date('m', $time)];
        $d = date('d', $time);
        $H = date('H', $time);
        $i = date('i', $time);
        $s = date('s', $time);
        $humanDate = locale()->get('locale:human_datetime_format');
        $humanDate = str_replace(array('{Y}', '{m}', '{d}', '{H}', '{i}', '{s}'), array($y, $m, $d, $H, $i, $s), $humanDate);
        return $humanDate;
    } else {
        return null;
    }
}

function locale_boolean($boolean) {
    if ($boolean) {
        return '<i class="fa fa-check text-success"></i>';
    } else {
        return '<i class="fa fa-times text-danger"></i>';
    }
}*/